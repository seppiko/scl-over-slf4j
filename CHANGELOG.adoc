= Seppiko Commons Logging over SLF4J

== 0.1.5 2024-04-27
. Update copyright
. Update SLF4J 2.0.13
. Update Seppiko Commons Logging 3.3.0
. Update OSSRH to Maven Central

== 0.1.4 2023-10-27
. Update document
. Update SLF4J 2.0.9
. Update Seppiko Commons Logging 3.2.0
. Add Copyright

== 0.1.3 2023-05-10
. Update Commons Logging 3.1.0
. Update document

== 0.1.2 2023-05-04
. Update SLF4J 2.0.7

== 0.1.1 2022-12-02
. Fixed bug

== 0.1.0 2022-12-02
. Implement over SLF4J

