package org.seppiko.logging.slf4j.test;

import org.junit.jupiter.api.Test;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingFactory;

/**
 * @author Leonard Woo
 */
public class SCLTests {

  record entity(
      int id,
      String name
  ) {
    @Override
    public String toString() {
      return "entity{" +
          "id=" + id +
          ", name='" + name + '\'' +
          '}';
    }
  }

  @Test
  public void sclTest() {
    Logging logging = LoggingFactory.getLogging("SCL-OVER-SLF4J");
    logging.info("logging test.");
    logging.atInfo().param(new entity(1, "admin")).message("TEST").log();
  }
}
