/*
 * Copyright 2024 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.seppiko.logging.slf4j;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Supplier;
import org.seppiko.commons.logging.Logging;
import org.seppiko.commons.logging.LoggingBuilder;
import org.slf4j.Logger;
import org.slf4j.event.Level;

/**
 * Commons Logging over SLF4J
 *
 * @author Leonard Woo
 */
public class SLF4JLogging implements Logging {

  private final Logger logger;

  /** Default constructor. */
  SLF4JLogging(Logger logger) {
    this.logger = logger;
  }

  /**
   * Convert level
   *
   * @param level Commons Logging Level
   * @return SLF4J Level instance
   */
  private Level getLevel(int level) {
    return switch (level) {
      case LEVEL_TRACE_INT -> Level.TRACE;
      case LEVEL_DEBUG_INT -> Level.DEBUG;
      case LEVEL_INFO_INT -> Level.INFO;
      case LEVEL_WARN_INT -> Level.WARN;
      case LEVEL_ERROR_INT -> Level.ERROR;
      case LEVEL_FATAL_INT -> throw new IllegalArgumentException("");
      default -> Level.intToLevel(level);
    };
  }

  /** {@inheritDoc} */
  @Override
  public String getName() {
    return logger.getName();
  }

  /** {@inheritDoc} */
  @Override
  public boolean isEnable(int level) {
    return switch (level) {
      case LEVEL_TRACE_INT -> logger.isTraceEnabled();
      case LEVEL_DEBUG_INT -> logger.isDebugEnabled();
      case LEVEL_INFO_INT -> logger.isInfoEnabled();
      case LEVEL_WARN_INT -> logger.isWarnEnabled();
      case LEVEL_ERROR_INT -> logger.isErrorEnabled();
      default -> throw new IllegalArgumentException("");
    };
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message) {
    log(level, message, (Exception) null);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Throwable cause) {
    log(level, message, cause, (Supplier<?>[]) null);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Supplier<?>... paramSuppliers) {
    log(level, message, null, paramSuppliers);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Object... params) {
    log(level, message, null, params);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Throwable cause, Supplier<?>... paramSuppliers) {
    Object[] params = null;
    if (paramSuppliers != null && paramSuppliers.length > 0) {
      params = Arrays.stream(paramSuppliers)
          .filter(Objects::nonNull)
          .map(Supplier::get)
          .toArray(Object[]::new);
    }

    log(level, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public void log(int level, CharSequence message, Throwable cause, Object... params) {
    var builder = logger.atLevel(getLevel(level));
    if (cause != null) {
      builder.setCause(cause);
    }
    if (params != null && params.length > 0) {
      Arrays.stream(params).toList()
          .stream()
          .filter(Objects::nonNull)
          .forEach(builder::addArgument);
    }
    builder.log(message.toString());
  }

  /** {@inheritDoc} */
  @Override
  public boolean isTraceEnable() {
    return isEnable(LEVEL_TRACE_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message) {
    log(LEVEL_TRACE_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Throwable cause) {
    log(LEVEL_TRACE_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Object... params) {
    log(LEVEL_TRACE_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_TRACE_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_TRACE_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void trace(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_TRACE_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atTrace() {
    return new SLF4JLoggingBuilder(this, LEVEL_TRACE_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isDebugEnable() {
    return isEnable(LEVEL_DEBUG_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message) {
    log(LEVEL_DEBUG_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Throwable cause) {
    log(LEVEL_DEBUG_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Object... params) {
    log(LEVEL_DEBUG_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_DEBUG_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_DEBUG_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void debug(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_DEBUG_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atDebug() {
    return new SLF4JLoggingBuilder(this, LEVEL_DEBUG_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isInfoEnable() {
    return isEnable(LEVEL_INFO_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message) {
    log(LEVEL_INFO_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Throwable cause) {
    log(LEVEL_INFO_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Object... params) {
    log(LEVEL_INFO_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_INFO_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_INFO_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void info(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_INFO_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atInfo() {
    return new SLF4JLoggingBuilder(this, LEVEL_INFO_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isWarnEnable() {
    return isEnable(LEVEL_WARN_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message) {
    log(LEVEL_WARN_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Throwable cause) {
    log(LEVEL_WARN_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Object... params) {
    log(LEVEL_WARN_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_WARN_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_WARN_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void warn(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_WARN_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atWarn() {
    return new SLF4JLoggingBuilder(this, LEVEL_WARN_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isErrorEnable() {
    return isEnable(LEVEL_ERROR_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message) {
    log(LEVEL_ERROR_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Throwable cause) {
    log(LEVEL_ERROR_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Object... params) {
    log(LEVEL_ERROR_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_ERROR_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_ERROR_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void error(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_ERROR_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atError() {
    return new SLF4JLoggingBuilder(this, LEVEL_ERROR_INT);
  }

  /** {@inheritDoc} */
  @Override
  public boolean isFatalEnable() {
    return isEnable(LEVEL_FATAL_INT);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message) {
    log(LEVEL_FATAL_INT, message);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Throwable cause) {
    log(LEVEL_FATAL_INT, message, cause);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Object... params) {
    log(LEVEL_FATAL_INT, message, params);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Supplier<?>... paramsSupplier) {
    log(LEVEL_FATAL_INT, message, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Throwable cause, Supplier<?>... paramsSupplier) {
    log(LEVEL_FATAL_INT, message, cause, paramsSupplier);
  }

  /** {@inheritDoc} */
  @Override
  public void fatal(CharSequence message, Throwable cause, Object... params) {
    log(LEVEL_FATAL_INT, message, cause, params);
  }

  /** {@inheritDoc} */
  @Override
  public LoggingBuilder atFatal() {
    return new SLF4JLoggingBuilder(this, LEVEL_FATAL_INT);
  }

}
